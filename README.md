#ValidationCase

Validation case module


## Install

```sh
$ npm install --save @adwatch/validationcase
```


## Usage

```js
import ValidationCase from '@adwatch/validationcase';
// or
var ValidationCase = require('@adwatch/validationcase/build');

//Initialization module
let validate = new ValidationCase();
```


## API


####check(elem, val, type, passConfig)

Set decision validation

**elem**

Type `node`

**val**

Type `string`

**type**

Type `string`

**passConfig**

*Only required in password input. In other cases set `''` or `null`*

Type `object`

Default passConfig = {minLength:6, analysis: ['hasUppercase', 'hasLowercase', 'hasDigits', 'hasSpecials']}

```js
let input = $('input[name="password1"]'),
	node = input[0],
	val = input.val();


console.log(validate.check(node, val, 'password', {minLength: 8, analysis: ['hasUppercase', 'hasLowercase']}));
```


####passBuffer

Get pass from buffer

```js
console.log(validate.check(node, val, 'password', {minLength: 8, analysis: ['hasUppercase']}));
console.log(validate.passBuffer);
```


## License

MIT ©
